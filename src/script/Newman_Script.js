const newman = require('newman');
newman.run({
    collection: require(`../collection/Check_Covid19.postman_collection.json`),
    environment: require(`../environment/CheckCovid_ENV.postman_environment.json`),
    reporters: ['htmlextra'], // report Type
    iterationCount: 1,
    reporter: {
      htmlextra: {
            export: `./src/report/newman.html`,
            // template: './template.hbs'
            // logs: true,
            // showOnlyFails: true,
            // noSyntaxHighlighting: true,
            // testPaging: true,
            browserTitle: "API-NEWMAN-REPORT",
            title: "API-NEWMAN-REPORT",
            // titleSize: 4,
            // omitHeaders: true,
            // skipHeaders: "Authorization",
            // hideRequestBody: ["Login"],
            // hideResponseBody: ["Auth Request"],
            // showEnvironmentData: true,
            // skipEnvironmentVars: ["API_KEY"],
            // showGlobalData: true,
            // skipGlobalVars: ["API_TOKEN"],
            // skipSensitiveData: true,
            // showMarkdownLinks: true,
            // showFolderDescription: true,
            // timezone: "Asia/Bangkok"
      }
    }

}).on('done', function (err, summary) {
    if (err) {
        throw err;
      }
    if (summary.run.failures !== 'undefined' && summary.run.failures.length != 0) {
        const output = {
          errors: [],
        };
        for (failure of summary.run.failures) {
          output.errors.push({
            name: failure.source.name,
            message: failure.error.message
          });
        }
        console.log(output);
    } else {
        console.log('newman run successful without errors');
    }
});